package com.thales.github

import com.thales.github.data.GithubRepository
import com.thales.github.data.Repository
import com.thales.github.data.remote.GithubService
import com.thales.github.utilities.BaseSchedulerProvider
import com.thales.github.utilities.SchedulerProvider
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Injection {
    private fun provideGithubService(): GithubService {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create<GithubService>(GithubService::class.java)
    }

    fun provideGithubRepository(): Repository {
        return GithubRepository(provideGithubService())
    }

    fun provideSchedulerProvider(): BaseSchedulerProvider {
        return SchedulerProvider.instance
    }
}
