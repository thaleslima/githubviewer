package com.thales.github.utilities

import android.content.Context
import android.net.ConnectivityManager

fun <T> Collection<T>?.isNullOrEmpty(): Boolean = this?.isEmpty() ?: true

fun Context.isNetworkAvailable(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = cm.activeNetworkInfo
    return info != null && info.isConnected && info.isAvailable
}