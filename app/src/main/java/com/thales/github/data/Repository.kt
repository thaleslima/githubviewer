package com.thales.github.data

import com.thales.github.model.User
import io.reactivex.Observable

interface Repository {
    fun getUserAndRepos(username: String): Observable<User>
}