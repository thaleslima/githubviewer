package com.thales.github.data.remote

import com.thales.github.model.Repo
import com.thales.github.model.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface GithubService {
    @GET("users/{login}")
    fun getUser(@Path("login") login: String): Observable<User>

    @GET("users/{login}/repos")
    fun getRepos(@Path("login") login: String): Observable<List<Repo>>
}