package com.thales.github.data

import com.thales.github.model.User
import io.reactivex.Observable
import com.thales.github.data.remote.GithubService

open class GithubRepository(private val githubService: GithubService): Repository {
    override fun getUserAndRepos(username: String): Observable<User> {
        return githubService.getUser(username).flatMap({
            githubService.getRepos(username)
        }, { user, repos ->
            user.repos = repos
            user
        })
    }
}