package com.thales.github.model

import com.google.gson.annotations.SerializedName

data class User(
        var login: String? = null,

        @SerializedName("avatar_url")
        var avatarUrl: String? = null,

        var name: String? = null,

        var company: String? = null,

        @SerializedName("repos_url")
        var reposUrl: String? = null,

        var repos: List<Repo>? = null
)