package com.thales.github.model

import com.google.gson.annotations.SerializedName

data class Repo(
        val id: Int = 0,

        val name: String? = null,

        @SerializedName("full_name")
        val fullName: String? = null,

        val description: String? = null,

        @SerializedName("stargazers_count")
        val stars: Int = 0,

        var language: String? = null
)
