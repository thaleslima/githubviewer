package com.thales.github.ui.main

interface MainContract {
    interface View {
        fun showSearchRequired()

        fun showProfile(username: String)
    }

    interface Presenter {
        fun searchUser(username: String)
    }
}
