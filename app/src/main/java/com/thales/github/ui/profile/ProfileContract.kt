package com.thales.github.ui.profile

import com.thales.github.model.Repo

interface ProfileContract {
    interface View {
        fun showUsername(username: String)

        fun showAvatar(avatarUrl: String)

        fun showRepos(repos: List<Repo>)

        fun showProgress()

        fun hideProgress()

        fun showUserNotFound()

        fun showNoConnection()

        fun showNoRepositoriesMessage()

        fun hasNetworkAvailable(): Boolean
    }

    interface Presenter {
        fun loadProfile(username: String)

        fun unsubscribe()
    }
}
