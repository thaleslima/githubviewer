package com.thales.github.ui.main

class MainPresenter(private val mView: MainContract.View) : MainContract.Presenter {
    override fun searchUser(username: String) {
        when (username.trim().isEmpty()) {
            true -> {
                mView.showSearchRequired()
            }
            else -> {
                mView.showProfile(username.trim())
            }
        }
    }
}