package com.thales.github.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.thales.github.R
import com.thales.github.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View {
    private var mPresenter: MainContract.Presenter = MainPresenter(this)

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    private fun setupView() {
        main_search_view.setOnClickListener {
            mPresenter.searchUser(main_username_view.text.toString())
        }
    }

    override fun showSearchRequired() {
        main_username_view.error = getString(R.string.main_username_required)
    }

    override fun showProfile(username: String) {
        ProfileActivity.navigate(this@MainActivity, username)
    }
}
