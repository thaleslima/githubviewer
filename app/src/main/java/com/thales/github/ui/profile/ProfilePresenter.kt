package com.thales.github.ui.profile

import android.annotation.SuppressLint
import com.thales.github.data.Repository
import com.thales.github.model.User
import com.thales.github.utilities.BaseSchedulerProvider
import com.thales.github.utilities.isNullOrEmpty
import io.reactivex.disposables.CompositeDisposable

class ProfilePresenter(private val mView: ProfileContract.View,
                       private val mRepository: Repository,
                       private val mProvider: BaseSchedulerProvider) : ProfileContract.Presenter {

    private val mSubscriptions: CompositeDisposable = CompositeDisposable()

    @SuppressLint("CheckResult")
    override fun loadProfile(username: String) {
        mView.showUsername(username)
        mView.showProgress()

        val subscription = mRepository.getUserAndRepos(username)
                .subscribeOn(mProvider.io())
                .observeOn(mProvider.ui())
                .subscribe({ data ->
                    showProfile(data)
                }, {
                    showError()
                })

        mSubscriptions.add(subscription)
    }

    private fun showProfile(data: User) {
        mView.hideProgress()

        data.avatarUrl?.let {
            mView.showAvatar(it)
        }

        if (data.repos.isNullOrEmpty()) {
            mView.showNoRepositoriesMessage()
        } else {
            mView.showRepos(data.repos!!)
        }
    }

    private fun showError() {
        mView.hideProgress()

        if (mView.hasNetworkAvailable()) {
            mView.showUserNotFound()
        } else {
            mView.showNoConnection()
        }
    }

    override fun unsubscribe() {
        this.mSubscriptions.clear()
    }

}
