package com.thales.github.ui.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.thales.github.R
import com.thales.github.model.Repo
import kotlinx.android.synthetic.main.item_list_repository.view.*
import java.util.ArrayList

class RepositoryAdapter : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    private val mValues: MutableList<Repo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_repository, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        with(holder) {
            description.text = item.name
            language.text = item.language
        }
    }

    fun replaceData(dataSet: List<Repo>) {
        setList(dataSet)
        notifyDataSetChanged()
    }

    private fun setList(dataSet: List<Repo>) {
        mValues.clear()
        mValues.addAll(dataSet)
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val description: TextView = mView.repository_description_view
        val language: TextView = mView.repository_language_view
    }
}
