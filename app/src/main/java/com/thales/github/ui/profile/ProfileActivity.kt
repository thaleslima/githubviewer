package com.thales.github.ui.profile

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.thales.github.Injection
import com.thales.github.R
import com.thales.github.model.Repo
import com.thales.github.utilities.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_profile.*
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions



class ProfileActivity : AppCompatActivity(), ProfileContract.View {
    private val mAdapter = RepositoryAdapter()
    private var mPresenter: ProfileContract.Presenter = ProfilePresenter(this,
            Injection.provideGithubRepository(),
            Injection.provideSchedulerProvider())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setupToolbar()
        setupRecyclerView()
        initialize()
    }

    public override fun onPause() {
        super.onPause()
        mPresenter.unsubscribe()
    }

    private fun initialize() {
        val username = intent.getStringExtra(EXTRA_USERNAME)
        mPresenter.loadProfile(username)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = null
    }

    private fun setupRecyclerView() {
        with(profile_recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    override fun showUsername(username: String) {
        profile_name_view.text = username
    }

    override fun showAvatar(avatarUrl: String) {
        val options = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)

        Glide.with(this)
                .load(avatarUrl)
                .apply(options)
                .into(profile_avatar_view)
    }

    override fun showRepos(repos: List<Repo>) {
        mAdapter.replaceData(repos)
    }

    override fun showProgress() {
        progress_bar.visibility = VISIBLE
    }

    override fun hideProgress() {
        progress_bar.visibility = GONE
    }

    override fun showUserNotFound() {
        showMessage(R.string.error_user_not_found)
    }

    override fun showNoConnection() {
        showMessage(R.string.error_connection)
    }

    private fun showMessage(@StringRes messageId: Int) {
        val builder = AlertDialog.Builder(this)

        builder.setMessage(messageId)
                .setTitle(R.string.app_name)
                .setPositiveButton(R.string.error_connection_ok) { dialog, _ -> dialog.dismiss() }

        val dialog = builder.create()
        dialog.show()
    }

    override fun hasNetworkAvailable(): Boolean {
        return isNetworkAvailable()
    }

    override fun showNoRepositoriesMessage() {
        profile_message_view.setText(R.string.profile_no_repositories)
        profile_message_view.visibility = VISIBLE
    }

    companion object {
        const val EXTRA_USERNAME = "extra_username"

        fun navigate(context: Context, username: String) {
            val intent = Intent(context, ProfileActivity::class.java)
            intent.putExtra(EXTRA_USERNAME, username)
            context.startActivity(intent)
        }
    }
}
