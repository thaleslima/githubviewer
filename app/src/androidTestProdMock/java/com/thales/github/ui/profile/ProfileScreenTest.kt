package com.thales.github.ui.profile

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.thales.github.R
import com.thales.github.utilities.MockServer.setDispatcherNoDataResponse200
import com.thales.github.utilities.MockServer.setDispatcherNoReposResponse200
import com.thales.github.utilities.MockServer.setDispatcherResponse200
import com.thales.github.utilities.MockServer.setDispatcherResponse404
import com.thales.github.utilities.MockServer.setDispatcherResponse500
import com.thales.github.utilities.MockServer.shutdown
import com.thales.github.utilities.MockServer.start
import com.thales.github.utilities.TestUtils.sleep
import org.hamcrest.core.IsNot.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class ProfileScreenTest {
    private val SEARCH = "google"
    private val SEARCH2 = "usernotfound123"

    private val REPO1 = "abpackage"
    private val REPO2 = "abstreet"

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(ProfileActivity::class.java, false, false)


    @Before
    @Throws(Exception::class)
    fun setup() {
        start()
    }

    @After
    @Throws(Exception::class)
    fun after() {
        shutdown()
        sleep()
    }

    @Test
    fun searchUser_displayedInUi() {
        setDispatcherResponse200()

        launchNewProfileActivity()

        // Verify user is displayed on screen
        onView(withId(R.id.profile_name_view)).check(matches(withText(SEARCH)))

        // Verify repositories is displayed on screen
        onView(withText(REPO1)).check(matches(isDisplayed()))
        onView(withText(REPO2)).check(matches(isDisplayed()))

        // Verify view is not displayed on screen
        onView(withId(R.id.profile_message_view)).check(matches(not(isDisplayed())))
        onView(withId(R.id.progress_bar)).check(matches(not(isDisplayed())))
    }


    @Test
    fun searchUser_noData_displayedInUi() {
        setDispatcherNoDataResponse200()

        launchNewProfileActivity()

        // Verify user is displayed on screen
        onView(withId(R.id.profile_name_view)).check(matches(withText(SEARCH)))

        // Check error text is displayed
        with(InstrumentationRegistry.getTargetContext()) {
            val expectedText = getString(R.string.profile_no_repositories)
            onView(withText(expectedText)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun searchUser_noRepos_displayedInUi() {
        setDispatcherNoReposResponse200()

        launchNewProfileActivity()

        // Verify view is displayed on screen
        onView(withId(R.id.profile_message_view)).check(matches(isDisplayed()))

        // Check error text is displayed
        with(InstrumentationRegistry.getTargetContext()) {
            val expectedText = getString(R.string.profile_no_repositories)
            onView(withText(expectedText)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun errorConnection_ShowsErrorUi() {
        setDispatcherResponse500()

        launchNewProfileActivity2()

        // Check error text is displayed
        with(InstrumentationRegistry.getTargetContext()) {
            val expectedText = getString(R.string.error_user_not_found)
            onView(withText(expectedText)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun userNotFound_ShowsErrorUi() {
        setDispatcherResponse404()

        launchNewProfileActivity()

        // Check error text is displayed
        with(InstrumentationRegistry.getTargetContext()) {
            val expectedText = getString(R.string.error_user_not_found)
            onView(withText(expectedText)).check(matches(isDisplayed()))
        }
    }

    private fun launchNewProfileActivity() {
        val intent = Intent(InstrumentationRegistry.getInstrumentation()
                .targetContext, ProfileActivity::class.java)
                .apply {
                    putExtra(ProfileActivity.EXTRA_USERNAME, SEARCH)
                }
        activityTestRule.launchActivity(intent)
    }

    private fun launchNewProfileActivity2() {
        val intent = Intent(InstrumentationRegistry.getInstrumentation()
                .targetContext, ProfileActivity::class.java)
                .apply {
                    putExtra(ProfileActivity.EXTRA_USERNAME, SEARCH2)
                }
        activityTestRule.launchActivity(intent)
    }
}
