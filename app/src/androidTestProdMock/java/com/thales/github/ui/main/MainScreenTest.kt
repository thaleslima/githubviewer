package com.thales.github.ui.main

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.thales.github.R
import com.thales.github.utilities.MockServer.setDispatcherResponse200
import com.thales.github.utilities.MockServer.shutdown
import com.thales.github.utilities.MockServer.start
import com.thales.github.utilities.TestUtils.sleep
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class MainScreenTest {
    private val SEARCH = "google"

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java, false, false)


    @Before
    @Throws(Exception::class)
    fun setup() {
        start()
    }

    @After
    @Throws(Exception::class)
    fun after() {
        shutdown()
        sleep()
    }

    @Test
    fun emptySearch_showsNonEmptyMessage() {
        launchNewUserActivity()

        // Try to search
        onView(ViewMatchers.withId(R.id.main_search_view)).perform(ViewActions.click())

        // Check empty message text is displayed
        with(InstrumentationRegistry.getTargetContext()) {
            val expectedText = getString(R.string.main_username_required)
            onView(withId(R.id.main_username_view)).check(matches(hasErrorText(expectedText)))
        }
    }

    @Test
    fun clickSearchButton_opensUserUi() {
        launchNewUserActivity()

        setDispatcherResponse200()

        // Add search
        onView(withId(R.id.main_username_view)).perform(typeText(SEARCH), closeSoftKeyboard())

        // Click to search
        onView(withId(R.id.main_search_view)).perform(click())

        // Verify user is displayed on screen
        onView(withId(R.id.profile_name_view)).check(matches(withText(SEARCH)))
    }

    private fun launchNewUserActivity() {
        val intent = Intent(InstrumentationRegistry.getInstrumentation().targetContext, MainActivity::class.java)
        activityTestRule.launchActivity(intent)
    }
}
