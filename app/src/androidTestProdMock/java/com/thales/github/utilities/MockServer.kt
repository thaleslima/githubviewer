package com.thales.github.utilities

import android.support.test.InstrumentationRegistry
import android.util.Log
import com.squareup.okhttp.mockwebserver.Dispatcher
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import com.squareup.okhttp.mockwebserver.RecordedRequest
import java.io.IOException

object MockServer {
    private val TAG = MockServer::class.java.name
    private const val FILE_NAME_USER_RESPONSE_200 = "response_user_200.json"
    private const val FILE_NAME_REPOS_RESPONSE_200 = "response_repos_200.json"
    private const val FILE_NAME_REPOS_NO_DATA_RESPONSE_200 = "response_repos_no_data_200.json"
    private const val FILE_NAME_USER_NO_DATA_RESPONSE_200 = "response_user_no_data_200.json"

    private const val API_URL_USERS = "users"
    private const val API_URL_REPOS = "repos"

    private lateinit var server: MockWebServer

    @Throws(IOException::class)
    fun start() {
        server = MockWebServer()
        server.start(2543)
    }

    fun shutdown() {
        try {
            server.shutdown()
        } catch (e: IOException) {
            Log.d(TAG, e.message)
        }
    }

    fun setDispatcherResponse200() {
        server.setDispatcher(createDispatcher200(FILE_NAME_USER_RESPONSE_200, FILE_NAME_REPOS_RESPONSE_200))
    }

    fun setDispatcherResponse404() {
        server.setDispatcher(DispatcherResponse404())
    }

    fun setDispatcherResponse500() {
        server.setDispatcher(DispatcherResponse500())
    }

    fun setDispatcherNoReposResponse200() {
        server.setDispatcher(createDispatcher200(FILE_NAME_USER_RESPONSE_200, FILE_NAME_REPOS_NO_DATA_RESPONSE_200))
    }

    fun setDispatcherNoDataResponse200() {
        server.setDispatcher(createDispatcher200(FILE_NAME_USER_NO_DATA_RESPONSE_200, FILE_NAME_REPOS_NO_DATA_RESPONSE_200))
    }

    private fun createDispatcher200(fileUser: String, fileRepo: String): Dispatcher {
        return object : Dispatcher() {
            @Throws(InterruptedException::class)
            override fun dispatch(request: RecordedRequest): MockResponse {
                if (request.path.contains(API_URL_REPOS)) {
                    return MockResponse()
                            .setResponseCode(200)
                            .setBody(StringUtil.getStringFromFile(InstrumentationRegistry.getInstrumentation().context, fileRepo))
                }

                if (request.path.contains(API_URL_USERS)) {
                    return MockResponse()
                            .setResponseCode(200)
                            .setBody(StringUtil.getStringFromFile(InstrumentationRegistry.getInstrumentation().context, fileUser))
                }

                throw InterruptedException()
            }
        }
    }

    private class DispatcherResponse500 : Dispatcher() {
        @Throws(InterruptedException::class)
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                    .setResponseCode(500)
                    .setBody("")
        }
    }

    private class DispatcherResponse404 : Dispatcher() {
        @Throws(InterruptedException::class)
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                    .setResponseCode(404)
                    .setBody("")
        }
    }
}