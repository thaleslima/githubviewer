package com.thales.github.utilities

import android.os.SystemClock

object TestUtils {
    fun sleep() {
        SystemClock.sleep(600)
    }
}