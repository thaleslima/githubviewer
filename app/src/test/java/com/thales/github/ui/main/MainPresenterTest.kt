package com.thales.github.ui.main

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MainPresenterTest {
    companion object {
        private const val SEARCH = "google"
        private const val SEARCH_EMPTY = ""
    }

    @Mock
    private lateinit var mView: MainContract.View

    private lateinit var mPresenter: MainPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun searchUser_emptySearchShowsErrorUi() {
        mPresenter = MainPresenter(mView)

        // When the presenter is asked to search an empty user
        mPresenter.searchUser(SEARCH_EMPTY)

        // Then an empty not error is shown in the UI
        verify<MainContract.View>(mView).showSearchRequired()
    }

    @Test
    fun searchUser_showsUserUi() {
        mPresenter = MainPresenter(mView)

        // When the presenter is asked to search an user
        mPresenter.searchUser(SEARCH)

        // Then User UI is displayed
        verify<MainContract.View>(mView).showProfile(SEARCH)
    }
}