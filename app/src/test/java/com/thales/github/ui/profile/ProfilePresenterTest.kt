package com.thales.github.ui.profile

import com.thales.github.data.Repository
import com.thales.github.model.Repo
import com.thales.github.model.User
import com.thales.github.utilities.BaseSchedulerProvider
import com.thales.github.utilities.ImmediateSchedulerProvider
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class ProfilePresenterTest {

    companion object {
        private const val SEARCH = "google"
        private const val SEARCH_EMPTY = ""
    }

    @Mock
    private lateinit var mView: ProfileContract.View

    @Mock
    private lateinit var mRepository: Repository

    private lateinit var mPresenter: ProfilePresenter
    private var mProvider: BaseSchedulerProvider = ImmediateSchedulerProvider()
    private val mUser = User(SEARCH, avatarUrl = "avatar")

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        val repos = ArrayList<Repo>()
        repos.add(Repo(1, "Repo 1", language = "java"))
        mUser.repos = repos
    }

    @Test
    fun searchUser_showsProfileUi() {
        Mockito.`when`(mRepository.getUserAndRepos(SEARCH)).thenReturn(Observable.just(mUser))

        mPresenter = ProfilePresenter(mView, mRepository, mProvider)

        // When the presenter is asked to search an user
        mPresenter.loadProfile(SEARCH)

        // Then data is shown in the UI
        verify<ProfileContract.View>(mView).showUsername(SEARCH)
        verify<ProfileContract.View>(mView).showProgress()
        verify<ProfileContract.View>(mView).hideProgress()
        verify<ProfileContract.View>(mView).showAvatar(mUser.avatarUrl!!)
        verify<ProfileContract.View>(mView).showRepos(mUser.repos!!)
    }

    @Test
    fun searchUser_noReposShowsProfileUi() {
        mUser.repos = ArrayList()
        Mockito.`when`(mRepository.getUserAndRepos(SEARCH)).thenReturn(Observable.just(mUser))

        mPresenter = ProfilePresenter(mView, mRepository, mProvider)

        // When the presenter is asked to search an user
        mPresenter.loadProfile(SEARCH)

        // Then data is shown in the UI
        verify<ProfileContract.View>(mView).showUsername(SEARCH)
        verify<ProfileContract.View>(mView).showProgress()
        verify<ProfileContract.View>(mView).hideProgress()
        verify<ProfileContract.View>(mView).showAvatar(mUser.avatarUrl!!)
        verify<ProfileContract.View>(mView).showNoRepositoriesMessage()
    }

    @Test
    fun searchUser_userNotFoundShowsErrorUi() {
        `when`(mRepository.getUserAndRepos(SEARCH_EMPTY)).thenReturn(Observable.error<User>(Exception()))
        `when`(mView.hasNetworkAvailable()).thenReturn(true)

        mPresenter = ProfilePresenter(mView, mRepository, mProvider)

        // When the presenter is asked to search an user not found
        mPresenter.loadProfile(SEARCH_EMPTY)

        verify<ProfileContract.View>(mView).showUsername(SEARCH_EMPTY)
        verify<ProfileContract.View>(mView).showProgress()
        verify<ProfileContract.View>(mView).hideProgress()

        // Then an error message is shown
        verify<ProfileContract.View>(mView).showUserNotFound()
    }

    @Test
    fun searchUser_noConnectionShowsErrorUi() {
        `when`(mRepository.getUserAndRepos(SEARCH_EMPTY)).thenReturn(Observable.error<User>(Exception()))
        `when`(mView.hasNetworkAvailable()).thenReturn(false)

        mPresenter = ProfilePresenter(mView, mRepository, mProvider)

        // When the presenter is asked to search an user not found
        mPresenter.loadProfile(SEARCH_EMPTY)

        verify<ProfileContract.View>(mView).showUsername(SEARCH_EMPTY)
        verify<ProfileContract.View>(mView).showProgress()
        verify<ProfileContract.View>(mView).hideProgress()

        // Then an error message is shown
        verify<ProfileContract.View>(mView).showNoConnection()
    }

}