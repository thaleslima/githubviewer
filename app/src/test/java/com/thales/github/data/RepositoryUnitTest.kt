package com.thales.github.data

import com.thales.github.data.remote.GithubService
import com.thales.github.model.Repo
import com.thales.github.model.User
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test

import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.ArrayList

class RepositoryUnitTest {
    private lateinit var repository: GithubRepository

    @Mock
    private lateinit var serviceService: GithubService

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = GithubRepository(serviceService)
    }

    @Test
    fun getUserAndRepos() {
        val username = "google"
        val user = User(username)
        val repos = ArrayList<Repo>()
        repos.add(Repo(1, "1"))

        `when`(serviceService.getUser(username)).thenReturn(Observable.just(user))
        `when`(serviceService.getRepos(username)).thenReturn(Observable.just(repos))

        val testObservable = TestObserver<User>()
        repository.getUserAndRepos(username).subscribe(testObservable)

        verify(serviceService).getRepos(username)
        verify(serviceService).getUser(username)

        user.repos = repos
        testObservable.assertValue(user)
    }
}
